---
title: "咖波相关网站/账号"
slug: "Account"
date: 2022-05-19T15:28:03+08:00
draft: false
---



原先是作者在巴哈姆特电玩资讯站个人小屋连载的自创角色，后于2015年韩国LINE Webtoon漫画比赛得到短篇创作组冠军，而在LINE Webtoon进行连载。

2020年3月已于Webtoon完结，现在在粉丝专页与Youtube网站继续短篇漫画与动画连载，以及创作各种贴图。

## 咖波 原条漫



## 咖波 新 GIF/条漫

目前，咖波

官方账号：

[https://twitter.com/bugcat_capoo_tw](https://twitter.com/bugcat_capoo_tw)


官方日本账号：

[https://twitter.com/capoo_jp/](https://twitter.com/capoo_jp/)


## 咖波 表情包

## 咖波周边

## 咖波 豆瓣小组

[#猫猫虫咖波系列（基本整理完毕，不定期更新）](https://www.douban.com/group/topic/243889382/?_i=2931424DXsKuDx)

[#猫猫虫咖波](https://www.douban.com/group/topic/243889286/?_i=2931441DXsKuDx)

## 咖波 TG 贴图

（需要越过高墙使用）

[Capoo Stickers](https://t.me/Capoo_Stickers/5)