import os
import re
from datetime import datetime
from datetime import timedelta
import time
files = os.listdir()

# for file in files:
for file in range(1,525):
        # print(file)
        # titles = re.split('第|話',file)
        # # print(titles)
        # try:
        #         episode = titles[1]
        # except:
        #         continue

        content = '---\ntitle: "{}"\nslug: "{}"\ndate: {}T15:45:23+08:00\ndraft: false\n---'.format(file,str(file),(datetime.now()-timedelta(days=file)).strftime('%Y-%m-%d'),)
        imgs = os.listdir(str(file))
        for i in range(1,len(imgs)-1):
                content = content +'\n\n![{}]({})'.format(str(i),'{}.jpg'.format(i))
        with open('{}/index.md'.format(file),'w') as f:
                f.write(content)
